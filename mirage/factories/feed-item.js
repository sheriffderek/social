import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  title() {
    return faker.lorem.sentence();
  },
  body() {
    return faker.lorem.paragraph();
  },
  pictureURL() {
    return faker.image.image(100, 100);
  },
  createdAt() {
    return faker.date.past();
  }
});
