import Ember from 'ember';
import { faker } from 'ember-cli-mirage';

export default Ember.Service.extend({
  _socket: null,
  newItems: Ember.computed(function () { return []; }),

  connect() {
    let socket = new FakeWebSocket('ws://foo.bar.baz');
    this.set('_socket', socket);
    socket.onmessage = (event) => {
      this.get('newItems').pushObject(event.data);
    };
  },
  disconnect() {
    this.get('_socket').close();
  },

  clear() {
    this.set('newItems', []);
  },

  _fireFakeNotification() {
    this.get('_socket')._fireFakeNotification();
  }
});

let FakeWebSocket = window.FakeWebSocket = function() {
  setTimeout(function() {
    if (this.onopen) { this.onopen({}); }
  }.bind(this));
};

window.FakeWebSocket.prototype = {
  send: function(data) {
    console.log("Server received: ", data);
    setTimeout(function() {
      if (this.onmessage) { this.onmessage({data: "OHAI!"}); }
    }.bind(this));
  },

  _fireFakeNotification: function() {
    setTimeout(function() {
      var event = {
        data: {
          type: 'feed-item',
          id: Date.now().toString(),
          attributes: {
            title: faker.lorem.sentence(),
            body: faker.lorem.paragraph(),
            'created-at': faker.date.recent(),
            'picture-url': faker.image.image(100, 100)
          }
        }
      };
      if (this.onmessage) { this.onmessage(event); }
    }.bind(this));
  },

  close: function() {
    // derp
  }
};
