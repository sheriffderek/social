import Ember from 'ember';

export default Ember.Route.extend({
  feed: Ember.inject.service(),

  model() {
    return this.store.findAll('feed-item');
  },

  activate() {
    this.get('feed').connect();
  },

  deactivate() {
    this.get('feed').disconnect();
  }
});
