import Ember from 'ember';

export default Ember.Controller.extend({
  feed: Ember.inject.service(),

  newItems: Ember.computed.readOnly('feed.newItems'),

  actions: {
    showNewItems() {
      let newItems = this.get('newItems');
      newItems.forEach(newItem => {
        this.store.pushPayload('data', {data: newItem});
      });
      this.get('feed').clear();
    },

    _fireFakeNotification() {
      this.get('feed')._fireFakeNotification();
    }
  }
});
